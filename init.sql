--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1 (Debian 15.1-1.pgdg110+1)
-- Dumped by pg_dump version 15.1

-- Started on 2022-12-30 03:34:51 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 215 (class 1259 OID 17169)
-- Name: userModels; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public."userModels" (
    id integer NOT NULL,
    fio character varying(255) NOT NULL,
    "dateOfBtd" timestamp with time zone NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."userModels" OWNER TO root;

--
-- TOC entry 214 (class 1259 OID 17168)
-- Name: userModels_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public."userModels_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."userModels_id_seq" OWNER TO root;

--
-- TOC entry 3328 (class 0 OID 0)
-- Dependencies: 214
-- Name: userModels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public."userModels_id_seq" OWNED BY public."userModels".id;


--
-- TOC entry 3176 (class 2604 OID 17172)
-- Name: userModels id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public."userModels" ALTER COLUMN id SET DEFAULT nextval('public."userModels_id_seq"'::regclass);


--
-- TOC entry 3322 (class 0 OID 17169)
-- Dependencies: 215
-- Data for Name: userModels; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public."userModels" (id, fio, "dateOfBtd", "createdAt", "updatedAt") FROM stdin;
1	Закатимов Егор Сергеевич	2005-01-20 21:00:00+00	2022-12-30 13:12:31.235+00	2022-12-30 13:12:31.235+00
2	Закатимов Егор Александрович	2006-06-19 20:00:00+00	2022-12-30 13:12:31.261+00	2022-12-30 13:12:31.261+00
3	Закатимов Егор Викторович	2003-04-09 20:00:00+00	2022-12-30 13:12:31.273+00	2022-12-30 13:12:31.273+00
4	Хаметзянов Егор Сергеевич	2008-11-20 21:00:00+00	2022-12-30 13:12:31.292+00	2022-12-30 13:12:31.292+00
5	Хаметзянов Егор Александрович	2003-11-08 21:00:00+00	2022-12-30 13:12:31.316+00	2022-12-30 13:12:31.316+00
6	Хаметзянов Егор Викторович	2005-03-01 21:00:00+00	2022-12-30 13:12:31.331+00	2022-12-30 13:12:31.331+00
7	Шматов Егор Сергеевич	2001-02-07 21:00:00+00	2022-12-30 13:12:31.362+00	2022-12-30 13:12:31.362+00
8	Шматов Егор Александрович	2008-02-23 21:00:00+00	2022-12-30 13:12:31.374+00	2022-12-30 13:12:31.374+00
9	Шматов Егор Викторович	2001-09-13 20:00:00+00	2022-12-30 13:12:31.388+00	2022-12-30 13:12:31.388+00
10	Закатимов Сергей Сергеевич	2001-05-31 20:00:00+00	2022-12-30 13:12:31.402+00	2022-12-30 13:12:31.402+00
11	Закатимов Сергей Александрович	2005-05-24 20:00:00+00	2022-12-30 13:12:31.416+00	2022-12-30 13:12:31.416+00
12	Закатимов Сергей Викторович	2004-11-09 21:00:00+00	2022-12-30 13:12:31.427+00	2022-12-30 13:12:31.427+00
13	Хаметзянов Сергей Сергеевич	2003-02-12 21:00:00+00	2022-12-30 13:12:31.439+00	2022-12-30 13:12:31.439+00
14	Хаметзянов Сергей Александрович	2005-06-08 20:00:00+00	2022-12-30 13:12:31.448+00	2022-12-30 13:12:31.448+00
15	Хаметзянов Сергей Викторович	2003-01-11 21:00:00+00	2022-12-30 13:12:31.459+00	2022-12-30 13:12:31.459+00
16	Шматов Сергей Сергеевич	2006-09-19 20:00:00+00	2022-12-30 13:12:31.469+00	2022-12-30 13:12:31.469+00
17	Шматов Сергей Александрович	2004-01-22 21:00:00+00	2022-12-30 13:12:31.48+00	2022-12-30 13:12:31.48+00
18	Шматов Сергей Викторович	2005-11-10 21:00:00+00	2022-12-30 13:12:31.49+00	2022-12-30 13:12:31.49+00
19	Закатимов Александр Сергеевич	2005-01-05 21:00:00+00	2022-12-30 13:12:31.5+00	2022-12-30 13:12:31.5+00
20	Закатимов Александр Александрович	2006-10-15 20:00:00+00	2022-12-30 13:12:31.51+00	2022-12-30 13:12:31.51+00
21	Закатимов Александр Викторович	2002-02-11 21:00:00+00	2022-12-30 13:12:31.521+00	2022-12-30 13:12:31.521+00
22	Хаметзянов Александр Сергеевич	2006-01-06 21:00:00+00	2022-12-30 13:12:31.535+00	2022-12-30 13:12:31.535+00
23	Хаметзянов Александр Александрович	2007-09-18 20:00:00+00	2022-12-30 13:12:31.547+00	2022-12-30 13:12:31.547+00
24	Хаметзянов Александр Викторович	2008-02-10 21:00:00+00	2022-12-30 13:12:31.556+00	2022-12-30 13:12:31.556+00
25	Шматов Александр Сергеевич	2005-08-06 20:00:00+00	2022-12-30 13:12:31.571+00	2022-12-30 13:12:31.571+00
26	Шматов Александр Александрович	2004-11-13 21:00:00+00	2022-12-30 13:12:31.583+00	2022-12-30 13:12:31.583+00
27	Шматов Александр Викторович	2007-06-13 20:00:00+00	2022-12-30 13:12:31.593+00	2022-12-30 13:12:31.593+00
\.


--
-- TOC entry 3329 (class 0 OID 0)
-- Dependencies: 214
-- Name: userModels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public."userModels_id_seq"', 27, true);


--
-- TOC entry 3178 (class 2606 OID 17174)
-- Name: userModels userModels_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public."userModels"
    ADD CONSTRAINT "userModels_pkey" PRIMARY KEY (id);


-- Completed on 2022-12-30 03:34:52 UTC

--
-- PostgreSQL database dump complete
--

