FROM node:latest

COPY ./ ./

RUN npm i

RUN mkdir output

CMD [ "node", "practice.js" ]

