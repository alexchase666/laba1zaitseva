const Sequelize = require("sequelize");
const { writeFileSync } = require("fs");
const path = require("path");
const XLSX = require("xlsx");

// // Part 1

const filePathForTask1 = path.resolve(__dirname, "output", "output1.json");

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}
let arr_en = [
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
  "g",
  "h",
  "i",
  "j",
  "k",
  "l",
  "m",
  "n",
  "o",
  "p",
  "q",
  "r",
  "s",
  "t",
  "u",
  "v",
  "w",
  "x",
  "y",
  "z",
];
let locates = ["google.com", "mail.ru", "rambler.ru", "yandex.ru", "random.ru"];

const getRandomString = (max) => {
  let str = "";
  for (let i = 0; i < max; i++) {
    str += arr_en[getRandomInt(arr_en.length - 1)];
  }
  return str;
};

const task1 = () => {
  let result = `${getRandomString(14)}@${
    locates[getRandomInt(locates.length - 1)]
  }`;

  writeFileSync(filePathForTask1, JSON.stringify(result));
};

task1()

//Part 2

const sequelize = new Sequelize("practice", "root", "qwerty", {
  host: "database",
  port: 5432,
  dialect: "postgres",
  logging: false,
});

const UserModel = sequelize.define("userModel", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  fio: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  dateOfBtd: {
    type: Sequelize.DATE,
    allowNull: false,
  },
});

const task2 = async () => {
  await sequelize.authenticate().then(async () => {
    await sequelize.sync();
    console.log("Connection with Data Base has been established successfully.");
  });

  let result = await UserModel.findAll({ raw: true });
  result = result.sort(function (a, b) {
    let temp1 = new Date(a.dateOfBtd);
    let temp2 = new Date(b.dateOfBtd);
    let c = temp1.getTime();
    let d = temp2.getTime();
    return c - d;
  });
  console.log(result);

  const workSheetColumnName = [
    "id",
    "fio",
    "dateOfBtd",
    "createdAt",
    "updatedAt",
  ];
  const workSheetName = 'Users'
  const filePath = "./output/output2.xlsx"


  const exportUsersToExcel = (result, workSheetColumnName, workSheetName, filePath) => {
    const data = result.map(res => {
      return [res.id, res.fio, res.dateOfBtd, res.createdAt, res.updatedAt];
    })
    const workBook = XLSX.utils.book_new();
    const workSheetData = [
      workSheetColumnName,
      ...data
    ]
    const workSheet = XLSX.utils.aoa_to_sheet(workSheetData)
    XLSX.utils.book_append_sheet(workBook, workSheet, workSheetName)
    XLSX.writeFile(workBook, path.resolve(filePath))
    return true
  }

  exportUsersToExcel(result, workSheetColumnName, workSheetName, filePath)
};

task2();
